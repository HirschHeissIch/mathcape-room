# MATHCAPE ROOM

A very simple Python tool to display a countdown and hints via a remote machine.

## Setting it up

* Connect a machine B (e.g. a Raspberry Pi) to the remote desktop
* Clone this repo to machine B
* Install Tkinter an machine B: `apt install python-tk`
* Install PIL on machine B: `apt install python3-pil python3-pil.imagetk`
* If needed edit the font Sizes `HINT_SIZE` and `CD_SIZE` in `mcr.py`
* On the controlling machine A:
  * Log on to machine B via SSH
  * cd to the location of `mcr.py`

## Run the MathCape Room

* On machine B run: `python3 mcr.py`. A text will be displayed in full screen stating that the starting signal is been awaited.

  *Note:*
    * When invoking the command from machine A, you will be able to see the remaining time as output in the terminal.
    * The use of Python 3 is necessary for this feature (at least the author couldn't make in work in Python 2 in less than 5 minutes and Python 2 will be [deprecated](https://pythonclock.org/) soon, anyways ...)
    * If you get an error `no display name and no $DISPLAY environment variable`, running `export DISPLAY=:0.0` should help.
* On machine A, edit the file `status` to stay `running` to start the countdount
* To display a now hint, just edit the file `hint`.
* To display an image, just add a file `hint_image.png`. Once it's deleted, the content of `hint` will be displayed again.

## Controlling via Telegram

For easier controlling of the script, run `python3 bot.zy` in paraller to `mcr.py`. You can then use [this bot](https://t.me/fermatslastbot) for commands:

* Send an image to set `hint_image.png`
* Send any text to set the hint
* Send `/start` to start the countdown
* Send `/reset` or `/end` to cancel the current run/reset the script to start
* Send `/remove_image` to remove `hint_image.png` and display the text stored in `hint`
* Send `/win` if the current team has won
