#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import tkinter as tk
import datetime
import pygame
from math import ceil
from PIL import ImageTk, Image
from playsound import playsound

# Set size for hints and countdown here
HINT_SIZE = 50
CD_SIZE = 50

# Set Countdown time here (in seconds)
TIME = 1800

# Init pygame for sound
pygame.mixer.init()

class App(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)

        ## Auxiliary variables
        self.hint = None
        self.hint_image = None
        self.hint_image_edit_date = None
        self.update_cd = True
        self.status = None

        ## Start in full screen
        ## Escape full srceen by F11 or Esc
        self.fs_state = False
        self.bind("<F11>", self.toggle_fullscreen)
        self.bind("<Escape>", self.end_fullscreen)
        self.toggle_fullscreen()

        ## Set up window
        self.frame = tk.Frame(self)
        self.frame.pack(side=tk.TOP, expand=tk.YES, fill=tk.BOTH)

        ## Prepare labels
        self.hints = tk.Label(
            self.frame,
            text="Verbleibende Zeit bis zur Räumung:",
            font=("Courier", HINT_SIZE),
        )
        self.cd_label=tk.Label(
            self.frame,
            text="",
            font=("Courier", CD_SIZE),
        )
        self.start_button = tk.Label(
            self.frame,
            text='Warte auf Startsignal ...',
            font=('Courier', HINT_SIZE),
        )

        ## Wait for start signal
        self.update()

        ## Start main loop
        self.mainloop()

    def update(self):
        """
        Update status and displayed content
        """
        with open('status', 'r') as file:
            status = file.read().strip('\n')
        if status == self.status:
            if self.status == 'running':
                self.show_hints()
                self.countdown()
        elif status == 'idle':
            self.hints.pack_forget()
            self.cd_label.pack_forget()
            self.start_button.pack(side=tk.TOP, expand=tk.YES)
            self.start_button.configure(text='Warte auf Startsignal ...')
            print('Game has not begun.', end='\r')
        elif status == 'running':
            self.start_button.pack_forget()
            self.hints.configure(text='Verbleibende Zeit bis zur Räumung:', image='')
            self.hints.pack(side='left', expand=True, fill="both")
            self.cd_label.pack(side='left', expand=True, fill="both")
            self.starttime = datetime.datetime.utcnow()
            self.show_hints()
            self.countdown()
        elif status == 'won':
            self.start_button.pack_forget()
            self.hints.pack(side='left', expand=True, fill="both")
            self.cd_label.pack(side='left', expand=True, fill="both")
            self.hints.configure(text='GEWONNEN!', image='')
            self.win()
        elif status == 'lost':
            self.hints.pack_forget()
            self.cd_label.pack_forget()
            self.start_button.pack(side=tk.TOP, expand=tk.YES)
            self.start_button.configure(text='Die Zeit ist um!')
            self.game_over()

        self.status = status
        self.after(1000, self.update)

    def countdown(self):
        """
        Display Countdown
        """
        # Normal functionality within the game
        remaining = ceil(TIME - (datetime.datetime.utcnow() - self.starttime).total_seconds())

        if remaining <= 0:
            print('Remaining time: {:02d}:{:02d}'.format((remaining//60), (remaining%60)), end='\r')
            with open('status', 'w') as file:
                file.write('lost')
            self.game_over()
        else:
            self.cd_label.configure(text='{:02d}:{:02d}'.format((remaining//60), (remaining%60)))
            print('Remaining time: {:02d}:{:02d}'.format((remaining//60), (remaining%60)), end='\r')

            if remaining <= 10:
                self.hints.configure(background='red4')
                self.hints.configure(foreground='white')
                self.cd_label.configure(background='red4')
                self.cd_label.configure(foreground='white')
            elif remaining <=  0.25 * TIME:
                self.hints.configure(background='red2')
                self.cd_label.configure(background='red2')
            elif remaining <=  0.5 * TIME:
                self.hints.configure(background='yellow')
                self.cd_label.configure(background='yellow')

    def show_hints(self):
        """
        Show hints
        """
        # Try to get an image first
        currently_has_image = bool(self.hint_image)
        try:
            self.hint_image = ImageTk.PhotoImage(Image.open('hint_image.png'))
            self._hint_image = self.hint_image
            hint_image_edit_date = os.path.getmtime('hint_image.png')
        except:
            self.hint_image = None
        removed_image = currently_has_image and not self.hint_image

        # If there is an image, display it
        if self.hint_image:
            # Play Sound if image is new
            if not hint_image_edit_date == self.hint_image_edit_date:
                self.hint_sound()
            # Set Image
            self.hint_image_edit_date = hint_image_edit_date
            self.hints.configure(image = self.hint_image)
        # If there is no image, display text
        else:
            with open('hint', 'r') as file:
                hint = file.readlines()
                hint = [l.strip('\n') for l in hint if l != '']
                if len(hint) > 1:
                    hint = '\n'.join(hint)
                else:
                   hint = hint[0]
                # Only update hint if its new or the image was removed
                if not hint == self.hint or removed_image:
                    self.hint = hint
                    self.hints.configure(text=self.hint, image='')
                    self.hint_sound()

    def game_over(self):
        """
        Play Game Over sound
        """
        self.playsound('gameover.mp3')

    def win(self):
        """
        Play Win sound
        """
        self.playsound('win.mp3')

    def hint_sound(self):
        """
        Play Hint sound
        """
        self.playsound('hint.mp3')

    @classmethod
    def playsound(cls, filename):
        """
        Play sound via playsound or pygame (playsound doesn't work an Raspian)
        """
        try:
            playsound(filename)
        except:
            pygame.mixer.music.load(filename)
            pygame.mixer.music.play()
            while pygame.mixer.music.get_busy() is True:
                continue

    def toggle_fullscreen(self, event=None):
        """
        Toggole Full Screen
        """
        self.fs_state = not self.fs_state
        self.attributes("-fullscreen", self.fs_state)
        return "break"

    def end_fullscreen(self, event=None):
        """
        End Full Screen
        """
        self.fs_state = False
        self.attributes("-fullscreen", False)
        return "break"

if __name__ == "__main__":
    app = App()
